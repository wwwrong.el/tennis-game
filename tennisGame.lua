player = {
  new = function (self)
    self.__index = self
    return setmetatable({},self)
  end
}

tennisGame={}

function tennisGame:new(name1, name2)
  local o = {
    title="",
    p1name = name1,
    p2name = name2,
    p1points = 0,
    p2points = 0,
  }
  self.__index = self
  return setmetatable(o,self)
end

function tennisGame:won_point(name)
  if name == self.p1name then
    self.p1points = self.p1points + 1
  else
    self.p2points = self.p2points + 1
  end
end

-- tennisGame method 1
tennisGame1 = tennisGame:new()
tennisGame1.title = "Method1"

-- if clause
function tennisGame1:score()
  local result = ""
  local tmp_pt = 0
  if self.p1points == self.p2points then
    if self.p1points == 0 then
      result = "Love-All"
    elseif self.p1points == 1 then
      result = "Fifteen-All"
    elseif self.p1points == 2 then
      result = "Thirty-All"
    else
      result = "Deuce"
    end
  elseif self.p1points >= 4 or self.p2points >= 4 then
    local match_pt = self.p1points - self.p2points
    if match_pt == 1 then
      result = "Advantage "..self.p1name
    elseif match_pt == -1 then
      result = "Advantage "..self.p2name
    elseif match_pt >= 2 then
      result = "Win for "..self.p1name
    else
      result = "Win for "..self.p2name
    end
  else
    for i = 1, 2 do
      if i == 1 then
        tmp_pt = self.p1points
      else
        result = result.."-"
        tmp_pt = self.p2points
      end
      if tmp_pt == 0 then
        result = result.."Love"
      elseif tmp_pt == 1 then
        result = result.."Fifteen"
      elseif tmp_pt == 2 then
        result = result.."Thirty"
      else
        result = result.."Forty"
      end
    end
  end
  return result
end

-- tennisGame method 2
tennisGame2 = tennisGame:new()
tennisGame2.title = "Method2"

-- table
function tennisGame2:score()
  local pt_alias = {[0]="Love","Fifteen","Thirty","Forty"}
  local result = ""
  local max_pt = math.max(self.p1points, self.p2points)
  if max_pt < 3 then
    result = pt_alias[self.p1points].."-"..(self.p1points==self.p2points and "All" or pt_alias[self.p2points])
  else
    if self.p1points == self.p2points then
      result = "Deuce"
    elseif max_pt == 3 then
      result = pt_alias[self.p1points].."-"..pt_alias[self.p2points]
    else
      result = (math.abs(self.p1points-self.p2points)==1 and "Advantage " or "Win for ")..(self.p1points>self.p2points and self.p1name or self.p2name)
    end
  end
  return result
end

-- tennisGame method 3
tennisGame3 = tennisGame:new()
tennisGame3.title = "Method3"

function tennisGame3:score()
  local pt_alias={[0]="Love","Fifteen","Thirty","Forty"}
  local result=""
  if math.max(self.p1points, self.p2points) < 4 and self.p1points + self.p2points < 6 then
    result = pt_alias[self.p1points].."-"..(self.p1points==self.p2points and "All" or pt_alias[self.p2points])
  else
    if self.p1points==self.p2points then
      result = "Deuce"
    else
      result = (math.abs(self.p1points-self.p2points)==1 and "Advantage " or "Win for ")..(self.p1points>self.p2points and self.p1name or self.p2name)
    end
  end
  return result
end
