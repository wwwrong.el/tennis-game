data={
  {0, 0, "Love-All"},
  {1, 1, "Fifteen-All"},
  {2, 2, "Thirty-All"},
  {3, 3, "Deuce"},
  {4, 4, "Deuce"},
  
  {1, 0, "Fifteen-Love"},
  {0, 1, "Love-Fifteen"},
  {2, 0, "Thirty-Love"},
  {0, 2, "Love-Thirty"},
  {3, 0, "Forty-Love"},
  {0, 3, "Love-Forty"},
  {4, 0, "Win for player1"},
  {0, 4, "Win for player2"},
  
  {2, 1, "Thirty-Fifteen"},
  {1, 2, "Fifteen-Thirty"},
  {3, 1, "Forty-Fifteen"},
  {1, 3, "Fifteen-Forty"},
  {4, 1, "Win for player1"},
  {1, 4, "Win for player2"},

  {3, 2, "Forty-Thirty"},
  {2, 3, "Thirty-Forty"},
  {4, 2, "Win for player1"},
  {2, 4, "Win for player2"},
  
  {4, 3, "Advantage player1"},
  {3, 4, "Advantage player2"},
  {5, 4, "Advantage player1"},
  {4, 5, "Advantage player2"},
  {15, 14, "Advantage player1"},
  {14, 15, "Advantage player2"},

  {6, 4, "Win for player1"},
  {4, 6, "Win for player2"},
  {16, 14, "Win for player1"},
  {14, 16, "Win for player2"},
}

dofile('tennisGame.lua')

function testTennis(tGame, p1p, p2p)
  local g = tGame:new('player1', 'player2')
  for i = 0, math.max(p1p, p2p) do
    if i < p1p then
      g:won_point(g.p1name)
    end
    if i < p2p then
      g:won_point(g.p2name)
    end
  end
  return g
end

function test(tg, t)
  local t1, t2
  print(tg.title..":")
  print(
    '|No. |P1  |P2  |tscore'..
    string.rep(' ',21)..'|rscore'..
    string.rep(' ',21)..'|name1'..
    string.rep(' ',12)..'|name2'..
    string.rep(' ',12)..'|'
  )
  print(
    '|----+----+----+'..
    string.rep('-',27)..'+'..
    string.rep('-',27)..'+'..
    string.rep('-',17)..'+'..
    string.rep('-',17)..'|'
  )
  t1 = os.clock()
  for i, v in ipairs(data) do
    local game = testTennis(tg, v[1], v[2])
    local gscore = game:score()
    print(
      '|'..(i<10 and '  ' or ' ')..i..' |'..
      (v[1]<10 and '  ' or ' ')..v[1]..' |'..
      (v[2]<10 and '  ' or ' ')..v[2]..' |'..
      v[3]..(v[3]:len()<27 and string.rep(' ',27-v[3]:len()) or '')..'|'..
      gscore..(" (%s)"):format((v[3]==gscore and "Ok" or "XX"))..
      (gscore:len()<22 and string.rep(' ',22-gscore:len()) or '')..'|'..
      game.p1name..(game.p1name:len()<17 and string.rep(' ',17-game.p1name:len()) or '')..'|'..
      game.p2name..(game.p2name:len()<17 and string.rep(' ',17-game.p2name:len()) or '')..'|'
    )
  end
  t2=os.clock()
  t[tg.title]=t2-t1
  print(("-"):rep(17))
  print('time: '..t[tg.title])
end
local dt={}
test(tennisGame1, dt)
test(tennisGame2, dt)
test(tennisGame3, dt)
print(("-"):rep(17))
for k,v in pairs(dt) do
  print(k,v)
end
